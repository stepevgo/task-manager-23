package ru.t1.stepanishchev.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stepanishchev.tm.api.service.ICommandService;
import ru.t1.stepanishchev.tm.command.AbstractCommand;
import ru.t1.stepanishchev.tm.enumerated.Role;

public abstract class AbstractSystemCommand extends AbstractCommand {

    @NotNull
    public ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

    @Override
    @Nullable
    public Role[] getRoles() {
        return null;
    }

}