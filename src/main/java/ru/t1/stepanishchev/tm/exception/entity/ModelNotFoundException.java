package ru.t1.stepanishchev.tm.exception.entity;

public class ModelNotFoundException extends AbstractEntityNotFoundException {

    public ModelNotFoundException() {
        super("Error! Model not found.");
    }

}