package ru.t1.stepanishchev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stepanishchev.tm.api.repository.IProjectRepository;
import ru.t1.stepanishchev.tm.api.service.IProjectService;
import ru.t1.stepanishchev.tm.enumerated.Status;
import ru.t1.stepanishchev.tm.exception.entity.ProjectNotFoundException;
import ru.t1.stepanishchev.tm.exception.entity.StatusEmptyException;
import ru.t1.stepanishchev.tm.exception.field.*;
import ru.t1.stepanishchev.tm.model.Project;

public class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(@NotNull final IProjectRepository projectRepository) {
        super(projectRepository);
    }

    @Override
    @NotNull
    public Project create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty())
            throw new UserIdEmptyException();
        if (name == null || name.isEmpty())
            throw new NameEmptyException();
        if (description == null)
            throw new DescriptionEmptyException();
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        add(userId, project);
        return project;
    }

    @Override
    @NotNull
    public Project create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty())
            throw new UserIdEmptyException();
        if (name == null || name.isEmpty())
            throw new NameEmptyException();
        @NotNull final Project project = new Project();
        project.setName(name);
        add(userId, project);
        return project;
    }

    @Override
    @NotNull
    public Project updateById(@Nullable final String userId, @Nullable final String id, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty())
            throw new UserIdEmptyException();
        if (name == null || name.isEmpty())
            throw new NameEmptyException();
        @Nullable final Project project = findOneById(userId, id);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    @NotNull
    public Project updateByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty())
            throw new UserIdEmptyException();
        if (name == null || name.isEmpty())
            throw new NameEmptyException();
        @Nullable final Project project = findOneByIndex(userId, index);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    @NotNull
    public Project changeProjectStatusByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final Status status) {
        if (userId == null || userId.isEmpty())
            throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= repository.getSize())
            throw new IndexIncorrectException();
        if (status == null)
            throw new StatusEmptyException();
        @Nullable final Project project = findOneByIndex(userId, index);
        if (project == null)
            throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

    @Override
    @NotNull
    public Project changeProjectStatusById(@Nullable final String userId, @Nullable final String id, @Nullable final Status status) {
        if (userId == null || userId.isEmpty())
            throw new UserIdEmptyException();
        if (id == null || id.isEmpty())
            throw new ProjectIdEmptyException();
        if (status == null)
            throw new StatusEmptyException();
        @Nullable final Project project = findOneById(userId, id);
        if (project == null)
            throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

}