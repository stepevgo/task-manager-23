package ru.t1.stepanishchev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stepanishchev.tm.enumerated.ProjectSort;
import ru.t1.stepanishchev.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    @NotNull
    M add(@NotNull M model);

    @NotNull
    List<M> findAll();

    @NotNull
    List<M> findAll(@NotNull Comparator<M> comparator);

    @NotNull
    List<M> findAll(@Nullable ProjectSort sort);

    void removeAll(@Nullable Collection<M> collection);

    @Nullable
    M findOneById(@NotNull String id);

    @NotNull
    M findOneByIndex(@NotNull Integer index);

    @Nullable
    M removeOne(@NotNull M model);

    @NotNull
    M removeOneById(@NotNull String id);

    @NotNull
    M removeOneByIndex(@NotNull Integer index);

    void removeAll();

    boolean existsById(@NotNull String id);

    Integer getSize();

}