package ru.t1.stepanishchev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stepanishchev.tm.api.repository.IUserRepository;
import ru.t1.stepanishchev.tm.enumerated.Role;
import ru.t1.stepanishchev.tm.model.User;
import ru.t1.stepanishchev.tm.util.HashUtil;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    @NotNull
    public User create(@Nullable final String login, @NotNull final String password) {
        @Nullable final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        user.setRole(Role.USUAL);
        return add(user);
    }

    @Override
    @NotNull
    public User create(@Nullable final String login, @NotNull final String password, @NotNull final String email) {
        @Nullable final User user = create(login, password);
        user.setEmail(email);
        return user;
    }

    @Override
    @NotNull
    public User create(@Nullable final String login, @NotNull final String password, @NotNull final Role role) {
        @Nullable final User user = create(login, password);
        return user;
    }

    @Override
    @Nullable
    public User findByLogin(@NotNull final String login) {
        return models
                .stream()
                .filter(m -> login.equals(m.getLogin()))
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    public User findByEmail(@NotNull final String email) {
        return models
                .stream()
                .filter(m -> email.equals(m.getEmail()))
                .findFirst()
                .orElse(null);
    }

    @Override
    @NotNull
    public Boolean isLoginExist(@NotNull final String login) {
        return models
                .stream()
                .anyMatch(m -> login.equals(m.getLogin()));
    }

    @Override
    @NotNull
    public Boolean isEmailExist(@NotNull final String email) {
        return models
                .stream()
                .anyMatch(m -> email.equals(m.getEmail()));
    }

}